resource "aws_iam_role" "lambda" {
  name = "${var.app_name}-${var.function_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_policy" "access" {
  name        = "${var.app_name}-${var.function_name}-access"
  path        = "/"
  description = "Policy to allow ${var.app_name}-${var.function_name} to log to CloudWatch and access Pinpoint"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "ecs:DescribeServices",
        "ecs:ListClusters",
        "ecs:ListServices"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attachment" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.access.arn
}
