
resource "aws_alb" "main" {
  name            = "${var.app_name}-load-balancer"
  subnets         = var.subnets
  security_groups = [aws_security_group.lb.id]


  tags = merge(map(
    "Name", "${var.app_name}-load-balancer",
    "Terraform", regex("[^/]+/terraform.*", path.cwd),
  ), var.tags)

}

resource "aws_alb_target_group" "app" {
  name        = "${var.app_name}-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "lambda"


  tags = merge(map(
    "Name", "${var.app_name}-target-group",
    "Terraform", regex("[^/]+/terraform.*", path.cwd),
  ), var.tags)

}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.main.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }

}

resource "aws_lambda_permission" "with_lb" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.backend.arn
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = aws_alb_target_group.app.arn
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_alb_target_group.app.arn
  target_id        = aws_lambda_function.backend.arn
  depends_on       = [aws_lambda_permission.with_lb]
}

resource "aws_security_group" "lb" {
  name        = "${var.app_name}-load-balancer-security-group"
  description = "controls access to the ALB"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = merge(map(
    "Name", "${var.app_name}-load-balancer-security-group",
    "Terraform", regex("[^/]+/terraform.*", path.cwd),
  ), var.tags)

}
