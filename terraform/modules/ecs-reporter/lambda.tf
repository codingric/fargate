resource "aws_lambda_function" "backend" {
  function_name    = "${var.app_name}-${var.function_name}"
  role             = aws_iam_role.lambda.arn
  runtime          = var.runtime
  handler          = var.handler
  filename         = var.filename
  source_code_hash = filebase64sha256(var.filename)

  tags = merge(map(
    "Name", "${var.app_name}-${var.function_name}",
    "Terraform", regex("[^/]+/terraform.*", path.cwd),
  ), var.tags)
}

output "lambda_arn" {
  value = aws_lambda_function.backend.arn
}

output "function_name" {
  value = var.function_name
}
