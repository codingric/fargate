variable "function_name" {
  default = "ecs-reporter"
}

variable "handler" {
  default = "reporter.ecs_report"
}
variable "filename" {
  type = string
}

variable "app_name" {
  description = "Application this backend belongs to"
}

variable "runtime" {
  default = "python3.8"
}

variable "tags" {
  type        = map(string)
  description = "(optional) Additional tags"
  default     = {}
}

variable "subnets" {}
variable "vpc_id" {}
