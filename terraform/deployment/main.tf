terraform {
  backend "s3" {
    bucket  = "terraform-f3e4rx"
    key     = "statefiles/deployment.tfstate"
    region  = "ap-southeast-2"
    profile = "admin"
  }
}

provider "aws" {
  profile = "admin"
  region  = "ap-southeast-2"
}


data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "reporter" {
  source = "../modules/ecs-reporter"

  app_name = "frankie-test"

  filename = "../../ecs-reporter/build.zip"

  vpc_id = data.aws_vpc.default.id

  subnets = data.aws_subnet_ids.all.ids

  tags = var.tags
}
/*
module "vpc" {
  source = "../modules/vpc"

  app_name = var.app_name

  tags = var.tags
}

module "ecs-app" {
  source = "../modules/ecs-app"

  app_name            = var.app_name
  app_image           = "containous/whoami"
  app_port            = 80
  app_template        = "app.tmpl"
  vpc_id              = module.vpc.vpc_id
  public_subnet_ids   = module.vpc.public_subnets[*].id
  private_subnet_ids  = module.vpc.private_subnets[*].id
  aws_region          = "ap-southeast-2"
  allowed_cidr_blocks = ["118.127.118.104/32"]

  tags = var.tags
}*/
