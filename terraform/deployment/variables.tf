variable "app_name" {
  type    = string
  default = "frankie-test"
}

variable "tags" {
  default = {
    "environment" = "test",
    "owner"       = "Ricardo Salinas",
    "contact"     = "ricardo@salinas.id.au"
  }
}
