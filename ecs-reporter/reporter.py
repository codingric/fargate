import boto3
import json

client = boto3.client('ecs')

def ecs_report(event, context): 
    clusters = []
    report = {"services":[]}
    nextToken = ""
    tag = []

    print("event", json.dumps(event))

    if event and 'tag' in event.get('queryStringParameters', {}):
        tag = event['queryStringParameters']['tag']

    while True:
        response = client.list_clusters(
            maxResults=100,
            nextToken=nextToken
        )
        for arn in response["clusterArns"]:
            clusters.append(arn)
        
        if "nextToken" in response:
            nextToken = response["nextToken"]
            continue
        
        break

    for cluster in clusters:
        nextToken = ""
        while True:
            response = client.list_services(
                cluster=cluster,
                nextToken=nextToken,
                launchType='FARGATE',
                maxResults=100,
                schedulingStrategy='REPLICA'
            )

            if response['serviceArns']:
    
                resp_svc= client.describe_services(
                    cluster=cluster,
                    services = response['serviceArns'],
                    include=['TAGS']
                )

                for service in resp_svc['services']:
                    report['services'].append({
                        "name": service["serviceName"],
                        "pending": service["runningCount"],
                        "running": service['pendingCount'],
                        "tags": service.get('tags',[])
                    })
                
            if "nextToken" in response:
                nextToken = response["nextToken"]
                continue
            
            break
    return {
        "statusCode": 200,
        "statusDescription": "200 OK",
        "isBase64Encoded": False,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps(report)
    }    

if __name__ == "__main__":
    print(ecs_report(None, None))