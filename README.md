# Test Fargate application

## ECS Reporter

This application reports on ESC services it has access to, it takes a GET parameter tags to filter to show only services that contain that tag.

The application can be packaged by running `build.sh`:

```
cd ecs-reporter
./build.sh
```

This will produce a zip file that can be deployed with terraform.

## Terraform scipts

These scripts contain terrafrom modules that can deploy the following:

- ecs-reporter (lambda function accessable via a Application Load Balancer)
- ecs-app (A ECS cluster that deploys a service running which ever image you like)

### Requirements
 
- Terrafrom v0.12
- AWS CLI
- Preconfigured AWS profile named `admin`

### Deployment

```
cd terraform/deployment
terraform init
terraform apply
```

### Configuration

Some configuratio changes can be made to `variables.tf` or `main.tf`